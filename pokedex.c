#include <stdio.h>
#include <stdlib.h>  
#include <string.h>

typedef struct Pokedex {
  char name[50];
  char type[50];
  char ability[50];
  /*I struggled to implemnt the Pokedex struct */
} Pokedex;

/*PokemonNode struct is created*/
typedef struct PokemonNode {
  char name[50];
  char type[50];
  char ability[50];
} PokemonNode;

/*PlayerNode Struct is created*/
typedef struct PlayerNode {
  char playername[50];
  char PokemonOwned[50];
} PlayerNode;

void PokemonList()
{
    printf( "Pokemon List called" );
}
void PlayerList()
{
    printf( "Player List called" );
}
void Functions()
{
    printf( "Functions called" );
}


     
int main()
{
  /*All pointers for all pokemon are created */
  PokemonNode bulbasaur;
  PokemonNode *bulbasaur_ptr;
  bulbasaur_ptr = &bulbasaur;

  PokemonNode charmander;
  PokemonNode *charmander_ptr;
  charmander_ptr = &charmander;

  PokemonNode squirtle;
  PokemonNode *squirtle_ptr;
  squirtle_ptr = &squirtle;

  PokemonNode butterfree;
  PokemonNode *butterfree_ptr;
  butterfree_ptr = &butterfree;

  PokemonNode pikipek;
  PokemonNode *pikipek_ptr;
  pikipek_ptr = &pikipek;

  PokemonNode venomoth;
  PokemonNode *venomoth_ptr;
  venomoth_ptr = &venomoth;

  PokemonNode piplup;
  PokemonNode *piplup_ptr;
  piplup_ptr = &piplup;

  PokemonNode ducklett;
  PokemonNode *ducklett_ptr;
  ducklett_ptr = &ducklett;

    /* All members of the struct are assigned their data*/
    strcpy( bulbasaur.name, "Bulbasaur");
    strcpy( bulbasaur.type, "Grass"); 
    strcpy( bulbasaur.ability, "Overgrow");

    strcpy( charmander.name, "Charmander");
    strcpy( charmander.type, "Fire"); 
    strcpy( charmander.ability, "Blaze");

    strcpy( squirtle.name, "Squirtle");
    strcpy( squirtle.type, "Water"); 
    strcpy( squirtle.ability, "Torrent");

    strcpy( butterfree.name, "Butterfree");
    strcpy( butterfree.type, "Bug"); 
    strcpy( butterfree.ability, "Compound Eyes");

    strcpy( pikipek.name, "Pikipek");
    strcpy( pikipek.type, "Normal"); 
    strcpy( pikipek.ability, "Keen Eyes");

    strcpy( venomoth.name, "Venomoth");
    strcpy( venomoth.type, "Bug"); 
    strcpy( venomoth.ability, "Shield Dust");

    strcpy( piplup.name, "Piplup");
    strcpy( piplup.type, "Water"); 
    strcpy( piplup.ability, "Torrent");

    strcpy( ducklett.name, "Ducklett");
    strcpy( ducklett.type, "Water"); 
    strcpy( ducklett.ability, "Keen Eye");

    int userinput;
 
    /*Menu switch cases */
    printf("Welcome to the Pokedex!\n");
    printf("1. Pokemon List\n" );
    printf("2. Player List\n" );
    printf("3. Functions\n" );
    printf("4. Exit\n" );
    printf( "Please pick one of the numbered options: " );
    scanf( "%d", &userinput );
    switch ( userinput ) {
        case 1:
        /*This will print all pokemon in the list */
            printf("Pokemon List chosen\n");
            printf("Pokemon Name: %s\n", bulbasaur_ptr->name);
            printf("Pokemon Type: %s\n", bulbasaur_ptr->type);
            printf("Pokemon Ability: %s\n",bulbasaur_ptr->ability);
            printf("\n");
            printf("Pokemon Name: %s\n", charmander_ptr->name);
            printf("Pokemon Type: %s\n", charmander_ptr->type);
            printf("Pokemon Ability: %s\n",charmander_ptr->ability);
            printf("\n");
            printf("Pokemon Name: %s\n", squirtle_ptr->name);
            printf("Pokemon Type: %s\n", squirtle_ptr->type);
            printf("Pokemon Ability: %s\n",squirtle_ptr->ability);
            printf("\n");
            printf("Pokemon Name: %s\n", butterfree_ptr->name);
            printf("Pokemon Type: %s\n", butterfree_ptr->type);
            printf("Pokemon Ability: %s\n",butterfree_ptr->ability);
            printf("\n");
            printf("Pokemon Name: %s\n", pikipek_ptr->name);
            printf("Pokemon Type: %s\n", pikipek_ptr->type);
            printf("Pokemon Ability: %s\n",pikipek_ptr->ability);
            printf("\n");
            printf("Pokemon Name: %s\n", venomoth_ptr->name);
            printf("Pokemon Type: %s\n", venomoth_ptr->type);
            printf("Pokemon Ability: %s\n",venomoth_ptr->ability);
            printf("\n");
            printf("Pokemon Name: %s\n", piplup_ptr->name);
            printf("Pokemon Type: %s\n", piplup_ptr->type);
            printf("Pokemon Ability: %s\n",piplup_ptr->ability);
            printf("\n");
            printf("Pokemon Name: %s\n", ducklett_ptr->name);
            printf("Pokemon Type: %s\n", ducklett_ptr->type);
            printf("Pokemon Ability: %s\n",ducklett_ptr->ability);
               
            break;
        case 2:          
            PlayerList();
            break;
        case 3:         
            Functions();
            break;
        case 4:        
            printf( "Thanks for using the Pokedex!\n" );
            break;
        default:            
            printf( "Please try again and input one of the numbered options.\n" );
            break;
    }
    getchar();
}

 /*Linked list attempt, I couldnt find a way to implement a linked list so that it accepts chars */
 /* 
#include <stdlib.h>
#include <stdio.h>

struct node {
  int value;
  struct node* next;
};
typedef struct node node_t;

void printlist(node_t *head) {
  node_t *temporary = head;

  while (temporary != NULL) {
    printf("%d - ", temporary->value);
    temporary = temporary->next;
  }
  printf("\n");
}

node_t *create_new_node(int value){
    node_t *result = malloc(sizeof(node_t));
    result->value = value;
    result->next = NULL;
    return result;
}

int main() {
  node_t *head;
  node_t *tmp;

  tmp = create_new_node(56);
  head = tmp;
  tmp = create_new_node(8);
  tmp->next = head;
  head = tmp;
  tmp = create_new_node(34);
  tmp->next = head;
  head = tmp;

  printlist(head);

  return 0;
}
*/